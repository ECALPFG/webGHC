<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
	Design by TEMPLATED
	http://templated.co
	Released for free under the Creative Commons Attribution License
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>ECAL PFG plots</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />

<script>
function hideexpand(me){
  if (me.style.height == 'auto'){
    me.style.height = '15px';
  } else {
    me.style.height = 'auto';
  }
}

</script>
<?php
$isuserresult = (strpos($_SERVER['REQUEST_URI'], 'result') == FALSE ? false : true);
$isusers = (strpos($_SERVER['REQUEST_URI'], 'users') == FALSE ? false : true);
$isusers = $isuserresult ? false : $isusers;
$isstatusbits = (strpos($_SERVER['REQUEST_URI'], 'EcalChannelStatus') == FALSE ? false : true);
?>
</head>
<body>
<div id="outer">
	<div id="header">
		<h1><a href="#">ECAL PFG plots</a></h1>
	</div>
	<div id="menu">
		<ul>
			<li><a href="https://gitlab.cern.ch/ECALPFG/webGHC" accesskey="5" title="">Source code</a></li>
		</ul>
	</div>
	<div id="content">
		<div id="primaryContentContainer">
			<div id="primaryContent">
			<?php
			include "body.php";
			?>
			</div>
		</div>
		<div id="secondaryContent">
			<?php
			include "leftsidebar.php";
			?>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">
		<p>Based on commit <a href="https://gitlab.cern.ch/ECALPFG/webGHC/commit/<?php echo exec('git log -n 1 --pretty=format:"%H"');?>"><?php echo exec('git log -n 1 --pretty=format:"%H"');?></a></p>
	</div>
</div>
</body>
</html>
